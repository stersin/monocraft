using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using MonoCraftClient.VoxelEngine.Lib.Components;


namespace MonoCraftClient.VoxelEngine.Components
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Debug : WorldDrawableComponent
    {
        #region Properties

        private SpriteFont _font;
        private int _udpatesPerSecond = 0;
        private int _framesPerSecond = 0;
        private KeyboardState _oldKeyboardState;
        private WeakReference _garbageCollectorRef;
        private long _totalMemory;

        #endregion

        #region GameComponent

        public Debug(Game game, World world)
            : base(game, world)
        {
            _font = game.Content.Load<SpriteFont>("Fonts/game");
            _garbageCollectorRef = new WeakReference(null);
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            _udpatesPerSecond = (int)Math.Floor(1 / gameTime.ElapsedGameTime.TotalSeconds);

            var currentKeyboardState = Keyboard.GetState();
            
            KeyboardState newKeyboardState = Keyboard.GetState();

            if (newKeyboardState.IsKeyDown(Keys.F1) && !_oldKeyboardState.IsKeyDown(Keys.F1))
            {
                World.ChunkManager.FillMode = World.ChunkManager.FillMode == FillMode.WireFrame ? FillMode.Solid : FillMode.WireFrame;
            }

            _oldKeyboardState = newKeyboardState;

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            _framesPerSecond = (int)Math.Floor(1 / gameTime.ElapsedGameTime.TotalSeconds);

            // if the weak reference is not alive anymore, we know garbage collector threw away our dummy object and then updated previously
            if (!_garbageCollectorRef.IsAlive)
            {
                // we assign a new dummy object to the weak reference, and then garbage collector will be triggered soon
                _garbageCollectorRef = new WeakReference(new Object());
                _totalMemory = GC.GetTotalMemory(false);
            }

            var strings = new List<String>();
            strings.Add(String.Format("FPS : {0} / UPS : {1} / Garbage Memory : {2}", 
                _framesPerSecond, 
                _udpatesPerSecond, 
                _totalMemory / (1024 * 1024)));
            strings.Add(String.Format("Loaded chunks : {0} / Setup chunks : {1} / Rendered chunks : {2}", World.ChunkManager.DebugLoadedChunks, World.ChunkManager.DebugSetupChunks, World.ChunkManager.DebugRenderedChunks));
            strings.Add(String.Format("Loaded blocks : {0} / Loaded vertices : {1} / Rendered vertices : {2}", World.ChunkManager.DebugLoadedBlocks, World.ChunkManager.DebugLoadedVertices, World.ChunkManager.DebugRenderedVertices));
            strings.Add(String.Format("Player chunk offset : {0}", World.ChunkManager.DebugCameraChunkOffset.ToString()));
            strings.Add(String.Format("Player block position : {0} / Player center position : {1}", World.Player.BlockPosition.ToString(),World.Player.Position.ToString()));
            strings.Add(String.Format("Player block aim : {0}", World.Player.BlockAim.ToString()));
            strings.Add(String.Format("Player look at : {0}", World.Player.Rotation.ToString()));

            World.GameScreen.ScreenManager.SpriteBatch.Begin();

            var height = 20;

            foreach(var str in strings)
            {
                World.GameScreen.ScreenManager.SpriteBatch.DrawString(_font, str, new Vector2(20, height), Color.White, 0, new Vector2(0, 0), 0.5f, new SpriteEffects(), 0);

                height += 20;
            }

            World.GameScreen.ScreenManager.SpriteBatch.End();

            base.Draw(gameTime);

            var state = new DepthStencilState();
            state.DepthBufferEnable = true;
            World.GameScreen.ScreenManager.GraphicsDevice.DepthStencilState = state;
        }

        #endregion
    }
}
