using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using MonoCraftClient.VoxelEngine;
using MonoCraftClient.VoxelEngine.Components.Blocks;
using MonoCraftClient.VoxelEngine.Components.Chunks;
using MonoCraftClient.VoxelEngine.Lib.Components;


namespace MonoCraftClient.VoxelEngine.Components.Entities
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Player : WorldDrawableComponent
    {
        #region Properties

        private const float ROTATION_SPEED = 0.1f;
        private const float MOVEMENT_FRICTION_FORCE = 20f;
        private const float MOVEMENT_WALK_FORCE = 7f;
        private const float MOVEMENT_RUN_FORCE = 10f;
        private const float MOVEMENT_JUMP_FORCE = 5f;

        private const float PLAYER_HEIGHT = 2.25f;
        private const float PLAYER_WIDTH = 0.25f;
        private const float PLAYER_DEPTH = 0.25f;

        private MouseState _mouseMoveState;
        private MouseState _mouseState;

        private Vector3 _position;
        private Vector3 _velocity;
        private float _yaw;
        private float _pitch;

        private bool _falling;

        private MouseState _prevMouseState;

        private BlockAccessor blockAccessor;

        public Vector3 Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
                Camera.Position = value + new Vector3(0, PLAYER_HEIGHT, 0);
            }
        }

        public Quaternion Rotation
        {
            get
            {
                return Camera.Rotation;
            }
            set
            {
                Camera.Rotation = value;
            }
        }

        public BoundingBox BoundingBox
        {
            get
            {
                return _getBoundingBoxFromPosition(_position);
            }
        }

        public Vector3 BlockPosition
        {
            get
            {
                return new Vector3((int)Position.X, (int)Position.Y, (int)Position.Z);
            }
        }

        public Vector3 BlockAim
        {
            get
            {
                var headDirection = Vector3.Transform(Vector3.Backward, Matrix.CreateFromQuaternion(Rotation));

                var curPos = World.Camera.Position;

                /*for (var i = 0; i < 4000; i++)
                {
                    curPos += headDirection * 0.005f;

                    if (_blockAccessor.MoveTo((int)curPos.X, (int)curPos.Y, (int)curPos.Z).IsSelectable)
                    {
                        return new Vector3((int)curPos.X, (int)curPos.Y, (int)curPos.Z);
                    }
                }*/

                return Vector3.Zero;
            }
        }

        public ICamera Camera
        {
            get;
            private set;
        }

        #endregion

        #region GameComponent



        public Player(Game game, World world, ICamera camera)
            : base(game, world)
        {
            _yaw = _pitch = 0;
            _position = Vector3.Zero;
            _velocity = Vector3.Zero;
            Camera = camera;
            _falling = true;

            blockAccessor = new BlockAccessor(world.ChunkManager);
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            Position = new Vector3(World.WorldConf.WorldWidth / 2, World.WorldConf.WorldMaxGroundHeight + 10, World.WorldConf.WorldDepth / 2);
            _falling = true;
            Rotate(45, 0);

            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            Vector3 position;

            float elapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            _updatePointOfView(elapsedTime);
            
            position = _getPositionAfterMovement(elapsedTime);
            position = _getPositionAfterCollisions(elapsedTime, position);
            
            Position = position;
                        
            #region Block action

            /*if(currentMouseState.LeftButton == ButtonState.Released 
                && _prevMouseState != null 
                && _prevMouseState.LeftButton == ButtonState.Pressed) 
            {
                var blockAim = BlockAim;
                blockAccessor
                    .MoveTo((int)blockAim.X, (int)blockAim.Y, (int)blockAim.Z)
                    .ReplaceWithBlock(new Block(Block.BlockType.None));
            }*/

            #endregion

            base.Update(gameTime);
        }
        
        public void Rotate(float yaw, float pitch)
        {
            _yaw += yaw;
            _pitch += pitch;

            // Locking camera rotation vertically between +/- 180 degrees
            if (_pitch < -1.55f)
                _pitch = -1.55f;
            else if (_pitch > 1.55f)
                _pitch = 1.55f;
            // End of locking

            Rotation = Quaternion.CreateFromYawPitchRoll(_yaw, _pitch, 0);
        }

        private void _updatePointOfView(float elapsedTime)
        {
            MouseState currentMouseState = Mouse.GetState();

            float mouseDX = currentMouseState.X - _mouseMoveState.X;
            float mouseDY = currentMouseState.Y - _mouseMoveState.Y;

            float yaw = 0;
            float pitch = 0;

            if (mouseDX != 0)
                yaw = -ROTATION_SPEED * (mouseDX * elapsedTime);

            if (mouseDY != 0)
                pitch = -ROTATION_SPEED * (mouseDY * elapsedTime);
            
            _mouseMoveState = new MouseState(GraphicsDevice.Viewport.Width / 2,
                    GraphicsDevice.Viewport.Height / 2,
                    0, ButtonState.Released, ButtonState.Released, ButtonState.Released, ButtonState.Released, ButtonState.Released);

            Mouse.SetPosition((int)_mouseMoveState.X, (int)_mouseMoveState.Y);
            _mouseState = Mouse.GetState();

            _prevMouseState = currentMouseState;

            Rotate(yaw, pitch);
        }

        private Vector3 _getPositionAfterMovement(float elapsedTime)
        {
            Vector3 position = Position;
            Vector3 acceleration = World.WorldConf.AirGravityAcceleration;

            KeyboardState currentKeyboardState = Keyboard.GetState();

            #region horizontal translation and velocity calculations
            Vector3 movementTranslation = Vector3.Zero;

            if (currentKeyboardState.IsKeyDown(Keys.Z))
                movementTranslation += Vector3.Forward;
            else if (currentKeyboardState.IsKeyDown(Keys.S))
                movementTranslation += Vector3.Backward;

            if (currentKeyboardState.IsKeyDown(Keys.Q))
                movementTranslation += Vector3.Left;
            else if (currentKeyboardState.IsKeyDown(Keys.D))
                movementTranslation += Vector3.Right;

            movementTranslation = Vector3.Transform(movementTranslation, Matrix.CreateFromQuaternion(Rotation));
                        
            // player is moving, so we apply velocity accordingly
            if (movementTranslation != Vector3.Zero)
            {
                float movementForce;

                if (currentKeyboardState.IsKeyDown(Keys.LeftShift))
                    movementForce = MOVEMENT_RUN_FORCE;
                else
                    movementForce = MOVEMENT_WALK_FORCE;

                movementTranslation.Y = 0;
                movementTranslation.Normalize();
                movementTranslation.X *= movementForce;
                movementTranslation.Z *= movementForce;

                _velocity.X = movementTranslation.X;
                _velocity.Z = movementTranslation.Z;
            }
            // player is on ground and stopped moving, so we apply movement inertia until he's completly stopped
            else if (!_falling)
            {
                if (_velocity.X > 0)
                {
                    _velocity.X -= MOVEMENT_FRICTION_FORCE * elapsedTime;

                    if (_velocity.X <= 0)
                        _velocity.X = 0;
                }
                else if (_velocity.X < 0)
                {
                    _velocity.X += MOVEMENT_FRICTION_FORCE * elapsedTime;

                    if (_velocity.X >= 0)
                        _velocity.X = 0;
                }

                if (_velocity.Z > 0)
                {
                    _velocity.Z -= MOVEMENT_FRICTION_FORCE * elapsedTime;

                    if (_velocity.Z <= 0)
                        _velocity.Z = 0;
                }
                else if (_velocity.Z < 0)
                {
                    _velocity.Z += MOVEMENT_FRICTION_FORCE * elapsedTime;

                    if (_velocity.Z >= 0)
                        _velocity.Z = 0;
                }
            }
            #endregion

            #region vertical translation and velocity calculations
                        
            // player is jumping, jump force applied to vertical velocity
            if (currentKeyboardState.IsKeyDown(Keys.Space) && _falling == false)
            {
                _falling = true;
                _velocity.Y = MOVEMENT_JUMP_FORCE;
            }

            // player is falling, gravity acceleration is applied
            if (_falling)
            {
                _velocity += acceleration * elapsedTime;
            }

            #endregion

            position += _velocity * elapsedTime;

            return position;
        }

        private Vector3 _getPositionAfterCollisions(float elapsedTime, Vector3 position)
        {
            blockAccessor.MoveTo(position);

            int mapX = (int)position.X;
            int mapY = (int)position.Y;
            int mapZ = (int)position.Z;

            var boundingBox = _getBoundingBoxFromPosition(position);

            // Checking Y collisions first
            if (blockAccessor.MoveTo(mapX, mapY, mapZ).Block.IsSolid)
            {
                if (blockAccessor.BoundingBox.Intersects(boundingBox))
                {
                    position.Y = mapY + 1;
                    _falling = false;
                }
            }
            else
            {
                if (position.Y < 0)
                {
                    position.Y = 0;
                    _falling = false;
                }
                else
                    _falling = true;
            }

            // a new Y coordinate could have been set so, let's set mapY again for horizontal checks
            mapY = (int)position.Y;
            
            if (blockAccessor.MoveTo(mapX, mapY, mapZ).Left.Block.IsSolid && blockAccessor.BoundingBox.Intersects(boundingBox))
            {
                position.X = mapX + PLAYER_WIDTH / 2;
                _velocity.X = 0;
            }
            else if (blockAccessor.MoveTo(mapX, mapY, mapZ).Right.Block.IsSolid && blockAccessor.BoundingBox.Intersects(boundingBox))
            {
                position.X = mapX + 1 - PLAYER_WIDTH / 2;
                _velocity.X = 0;
            }

            if (blockAccessor.MoveTo(mapX, mapY, mapZ).Backward.Block.IsSolid && blockAccessor.BoundingBox.Intersects(boundingBox))
            {
               position.Z = mapZ + PLAYER_DEPTH / 2;
               _velocity.Z = 0;
            }
            else if (blockAccessor.MoveTo(mapX, mapY, mapZ).Forward.Block.IsSolid && blockAccessor.BoundingBox.Intersects(boundingBox))
            {
                position.Z = mapZ + 1 - PLAYER_DEPTH / 2;
                _velocity.Z = 0;
            }

            return position;
        }

        private BoundingBox _getBoundingBoxFromPosition(Vector3 position)
        {
            return new BoundingBox(new Vector3(-PLAYER_WIDTH / 2, 0, -PLAYER_DEPTH / 2) + position, new Vector3(PLAYER_WIDTH / 2, PLAYER_HEIGHT, PLAYER_DEPTH / 2) + position);
        }

        #endregion

    }
}
