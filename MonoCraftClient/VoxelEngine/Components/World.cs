﻿using Microsoft.Xna.Framework;
using MonoCraftClient.Lib.Components;
using MonoCraftClient.ScreenManagement;
using MonoCraftClient.VoxelEngine;
using MonoCraftClient.VoxelEngine.Components.Cameras;
using MonoCraftClient.VoxelEngine.Components.Chunks;
using MonoCraftClient.VoxelEngine.Components.Entities;
using MonoCraftClient.VoxelEngine.Lib.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoCraftClient.VoxelEngine.Components
{
    public class World : ContainerComponent
    {
        #region Properties

        public WorldConf WorldConf { get; private set; }
        public ChunkManager ChunkManager { get; private set; }
        public Camera Camera { get; private set; }
        public Player Player { get; private set; }
        public GameScreen GameScreen { get; private set; }

        #endregion

        #region GameComponent

        public World(Game game, GameScreen gameScreen, WorldConf worldConf)
            : base(game)
        {
            GameScreen = gameScreen;
            WorldConf = worldConf;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {   
            //components.Add(Networking);
            components.Add(Camera = new Camera(Game, this));
            //components.Add(Map = new Map(Game, this));
            //components.Add(skyDome);
            components.Add(ChunkManager = new ChunkManager(Game, this, Camera));
            components.Add(Player = new Player(Game, this, Camera));
            components.Add(new Debug(Game, this));
            
            base.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();
        }


        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        /// <summary>
        /// Called when the DrawableGameComponent needs to be drawn. Override this method
        //  with component-specific drawing code.
        /// </summary>
        /// <param name="gameTime">Time passed since the last call to Draw.</param>
        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }

        #endregion
    }
}
