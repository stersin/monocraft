﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoCraftClient.VoxelEngine.Components.Blocks
{
    public struct Block
    {
        public enum BlockType : byte
        {
            Invalid,
            NotLoaded,
            Air,
            Rock,
            Dirt,
            Grass,
            Snow,
            Water
        }

        public BlockType Type;

        public Block(BlockType type)
        {
            Type = type;
        }

        public bool IsValid
        {
            get { return Type != BlockType.Invalid; }
        }

        public bool IsSolid
        {
            get { return IsValid && !IsLiquid && !IsAir; }
        }

        public bool IsLiquid
        {
            get { return Type == BlockType.Water; }
        }

        public bool IsAir
        {
            get { return Type == BlockType.Air; }
        }

        public bool IsTransparent
        {
            get { return IsLiquid || IsAir; }
        }

        public bool IsSelectable
        {
            get { return IsSolid; }
        }

        public Color Color
        {
            get
            {
                switch (Type)
                {
                    case BlockType.Rock:
                        return Color.Gray;
                    case BlockType.Dirt:
                        return Color.Brown;
                    case BlockType.Grass:
                        return Color.Green;
                    case BlockType.Snow:
                        return Color.White;
                    case BlockType.Water:
                        return Color.Aqua;
                    default:
                        return Color.Purple;
                }
            }
        }
    }
}