﻿using Microsoft.Xna.Framework;
using MonoCraftClient.VoxelEngine.Components.Chunks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoCraftClient.VoxelEngine.Components.Blocks
{
    public class BlockAccessor
    {
        public ChunkManager ChunkManager { get; private set; }
        public Block Block { get; private set; }
        public BoundingBox BoundingBox { get { return new BoundingBox(Position, new Vector3(Position.X + 1, Position.Y + 1, Position.Z + 1)); } }

        public Vector3 Position { get; private set; }
                
        public BlockAccessor(ChunkManager chunkManager)
        {
            ChunkManager = chunkManager;
            
            Block = new Block(Block.BlockType.Invalid);
        }

        public BlockAccessor MoveTo(int x, int y, int z)
        {
            Block = ChunkManager.GetBlockAtPosition(x, y, z);
            
            Position = new Vector3(x, y, z);
            
            return this;
        }

        public BlockAccessor MoveTo(Vector3 position)
        {
            return MoveTo((int)position.X, (int)position.Y, (int)position.Z);
        }

        public BlockAccessor Up
        {
            get
            {
                MoveTo(Position + Vector3.Up);
                return this;
            }
        }

        public BlockAccessor Down
        {
            get
            {
                MoveTo(Position + Vector3.Down);
                return this;
            }
        }

        public BlockAccessor Left
        {
            get
            {
                MoveTo(Position + Vector3.Left);
                return this;
            }
        }

        public BlockAccessor Right
        {
            get
            {
                MoveTo(Position + Vector3.Right);
                return this;
            }
        }

        public BlockAccessor Backward
        {
            get
            {
                MoveTo(Position + Vector3.Forward);
                return this;
            }
        }

        public BlockAccessor Forward
        {
            get
            {
                MoveTo(Position + Vector3.Backward);
                return this;
            }
        }
    }
}
