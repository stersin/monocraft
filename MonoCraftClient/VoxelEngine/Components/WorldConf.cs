﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoCraftClient.VoxelEngine.Components
{
    public class WorldConf
    {
        public Vector3 AirGravityAcceleration;
        public Vector3 LiquidGravityAcceleration;

        public short NumChunksWidth { get; private set; }
        public short NumChunksHeight { get; private set; }
        public short NumChunksDepth { get; private set; }

        public short ChunkWidth { get; private set; }
        public short ChunkHeight { get; private set; }
        public short ChunkDepth { get; private set; }

        public int WorldWidth { get { return NumChunksWidth * ChunkWidth; } }
        public int WorldHeight { get { return NumChunksHeight * ChunkHeight; } }
        public int WorldDepth { get { return NumChunksDepth * ChunkDepth; } }

        public int WorldMinGroundHeight { get { return WorldHeight / 4; } }
        public int WorldMaxGroundHeight { get { return WorldMinGroundHeight + WorldHeight / 4; } }

        public short ChunksToRenderRadius { get { return 3; } }
        public short ChunksToLoadRadius { get { return (short)(ChunksToRenderRadius + 1); } }

        public WorldConf()
        {
            AirGravityAcceleration = new Vector3(0, -10.0f, 0);
            LiquidGravityAcceleration = new Vector3(0, -5.0f, 0);

            NumChunksWidth = 20;
            NumChunksHeight = 1;
            NumChunksDepth = 20;

            ChunkWidth = 32;
            ChunkHeight = 128;
            ChunkDepth = 32;
        }
    }
}
