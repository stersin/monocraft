﻿using Microsoft.Xna.Framework;
using MonoCraftClient.VoxelEngine.Components.Blocks;
using MonoCraftClient.VoxelEngine.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoCraftClient.VoxelEngine.Components.Chunks
{
    public class ChunkMesh
    {
        public List<int> Indices { get; protected set; }
        public List<VertexPositionColorNormal> Vertices {get; protected set;}

        public Chunk Chunk { get; private set; }
        
        public ChunkMesh(Chunk chunk)
        {
            Chunk = chunk;

            Indices = new List<int>();
            Vertices = new List<VertexPositionColorNormal>();
        }

        public virtual void Build()
        {
            Clear();

            for (int x = 0; x < Chunk.World.WorldConf.ChunkWidth; x++)
            {
                for (int y = 0; y < Chunk.World.WorldConf.ChunkHeight; y++)
                {
                    for (int z = 0; z < Chunk.World.WorldConf.ChunkDepth; z++)
                    {
                        _BuildBlockMesh(Chunk.Blocks[x][y][z], x, y, z);
                    }
                }
            }
        }

        public void Clear()
        {
            Indices.Clear();
            Vertices.Clear();
        }


        private int _BuildBlockMesh(Block block, int inChunkX, int inChunkY, int inChunkZ)
        {
            if (block.IsAir)
                return 0;

            Vector3 worldPosition = Chunk.BoundingBox.Min + new Vector3(inChunkX, inChunkY, inChunkZ);

            Vector3 p1 = worldPosition + new Vector3(1, 1, 0);
            Vector3 p2 = worldPosition + new Vector3(1, 0, 0);
            Vector3 p3 = worldPosition + new Vector3(0, 0, 0);
            Vector3 p4 = worldPosition + new Vector3(0, 1, 0);
            Vector3 p5 = worldPosition + new Vector3(1, 1, 1);
            Vector3 p6 = worldPosition + new Vector3(0, 1, 1);
            Vector3 p7 = worldPosition + new Vector3(0, 0, 1);
            Vector3 p8 = worldPosition + new Vector3(1, 0, 1);

            Vector3 n;
            Microsoft.Xna.Framework.Color c = block.Color;
            int globalVertexIndex = Vertices.Count;
            int numCreatedVertices = 0;

            // Front face
            n = Vector3.Forward;

            if (Chunk.ChunkManager.GetBlockAtPosition(worldPosition + n).IsTransparent)
            {
                Vertices.Add(new VertexPositionColorNormal(p1, c, n));
                Vertices.Add(new VertexPositionColorNormal(p2, c, n));
                Vertices.Add(new VertexPositionColorNormal(p3, c, n));
                Vertices.Add(new VertexPositionColorNormal(p4, c, n));

                Indices.Add(globalVertexIndex + 0); Indices.Add(globalVertexIndex + 3); Indices.Add(globalVertexIndex + 2); //p1 => p4 => p3
                Indices.Add(globalVertexIndex + 0); Indices.Add(globalVertexIndex + 2); Indices.Add(globalVertexIndex + 1); //p1 => p3 => p2

                numCreatedVertices += 4;
                globalVertexIndex += 4;
            }

            // Back face
            n = Vector3.Backward;

            if (Chunk.ChunkManager.GetBlockAtPosition(worldPosition + n).IsTransparent)
            {

                Vertices.Add(new VertexPositionColorNormal(p5, c, n));
                Vertices.Add(new VertexPositionColorNormal(p6, c, n));
                Vertices.Add(new VertexPositionColorNormal(p7, c, n));
                Vertices.Add(new VertexPositionColorNormal(p8, c, n));

                Indices.Add(globalVertexIndex + 0); Indices.Add(globalVertexIndex + 3); Indices.Add(globalVertexIndex + 2); //p5 => p8 => p7
                Indices.Add(globalVertexIndex + 0); Indices.Add(globalVertexIndex + 2); Indices.Add(globalVertexIndex + 1); //p5 => p7 => p6

                numCreatedVertices += 4;
                globalVertexIndex += 4;
            }

            // Right face
            n = Vector3.Right;

            if (Chunk.ChunkManager.GetBlockAtPosition(worldPosition + n).IsTransparent)
            {
                Vertices.Add(new VertexPositionColorNormal(p1, c, n));
                Vertices.Add(new VertexPositionColorNormal(p5, c, n));
                Vertices.Add(new VertexPositionColorNormal(p8, c, n));
                Vertices.Add(new VertexPositionColorNormal(p2, c, n));

                Indices.Add(globalVertexIndex + 0); Indices.Add(globalVertexIndex + 3); Indices.Add(globalVertexIndex + 2); // p1 => p2 => p8
                Indices.Add(globalVertexIndex + 0); Indices.Add(globalVertexIndex + 2); Indices.Add(globalVertexIndex + 1); // p1 => p8 => p5

                numCreatedVertices += 4;
                globalVertexIndex += 4;
            }

            // Left face
            n = Vector3.Left;

            if (Chunk.ChunkManager.GetBlockAtPosition(worldPosition + n).IsTransparent)
            {
                Vertices.Add(new VertexPositionColorNormal(p3, c, n));
                Vertices.Add(new VertexPositionColorNormal(p7, c, n));
                Vertices.Add(new VertexPositionColorNormal(p6, c, n));
                Vertices.Add(new VertexPositionColorNormal(p4, c, n));

                Indices.Add(globalVertexIndex + 0); Indices.Add(globalVertexIndex + 3); Indices.Add(globalVertexIndex + 2); // p3 => p4 => p6
                Indices.Add(globalVertexIndex + 0); Indices.Add(globalVertexIndex + 2); Indices.Add(globalVertexIndex + 1); // p3 => p6 => p7

                numCreatedVertices += 4;
                globalVertexIndex += 4;
            }

            // Top face
            n = Vector3.Up;

            if (Chunk.ChunkManager.GetBlockAtPosition(worldPosition + n).IsTransparent)
            {
                Vertices.Add(new VertexPositionColorNormal(p5, c, n));
                Vertices.Add(new VertexPositionColorNormal(p1, c, n));
                Vertices.Add(new VertexPositionColorNormal(p4, c, n));
                Vertices.Add(new VertexPositionColorNormal(p6, c, n));

                Indices.Add(globalVertexIndex + 0); Indices.Add(globalVertexIndex + 3); Indices.Add(globalVertexIndex + 2); // p5 => p6 => p4
                Indices.Add(globalVertexIndex + 0); Indices.Add(globalVertexIndex + 2); Indices.Add(globalVertexIndex + 1); // p5 => p4 => p1

                numCreatedVertices += 4;
                globalVertexIndex += 4;
            }

            // Bottom face
            n = Vector3.Down;

            if (Chunk.ChunkManager.GetBlockAtPosition(worldPosition + n).IsTransparent)
            {
                Vertices.Add(new VertexPositionColorNormal(p2, c, n));
                Vertices.Add(new VertexPositionColorNormal(p8, c, n));
                Vertices.Add(new VertexPositionColorNormal(p7, c, n));
                Vertices.Add(new VertexPositionColorNormal(p3, c, n));

                Indices.Add(globalVertexIndex + 0); Indices.Add(globalVertexIndex + 3); Indices.Add(globalVertexIndex + 2); // p2 => p3 => p7
                Indices.Add(globalVertexIndex + 0); Indices.Add(globalVertexIndex + 2); Indices.Add(globalVertexIndex + 1); // p2 => p7 => p8

                numCreatedVertices += 4;
                globalVertexIndex += 4;
            }

            return numCreatedVertices;
        }
    }
}
