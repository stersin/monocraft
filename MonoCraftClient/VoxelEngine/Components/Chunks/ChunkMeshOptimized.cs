﻿using Microsoft.Xna.Framework;
using MonoCraftClient.VoxelEngine.Components.Blocks;
using MonoCraftClient.VoxelEngine.Lib;
using Poly2Tri;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClipperLib;

namespace MonoCraftClient.VoxelEngine.Components.Chunks
{
    using ClipperPolygon = List<IntPoint>;

    public class ChunkMeshOptimized : ChunkMesh
    {
        private Dictionary<VertexPositionColorNormal,int> _verticesToIndices;
        private int _globalVertexIndex;

        private class Poly2TriPolygonFromClipper : Polygon
        {
            private ChunkMeshOptimized _chunkMesh;

            public static Poly2TriPolygonFromClipper Factory(ChunkMeshOptimized chunkMesh, ClipperPolygon clipperPolygon, List<ClipperPolygon> clipperPolygonHoles)
            {
                IList<PolygonPoint> polygonPoints = new List<PolygonPoint>();

                foreach (IntPoint intPoint in clipperPolygon)
                {
                    polygonPoints.Add(new PolygonPoint(intPoint.X, intPoint.Y));
                }

                return new Poly2TriPolygonFromClipper(polygonPoints, chunkMesh, clipperPolygon, clipperPolygonHoles);
            }

            private Poly2TriPolygonFromClipper(IList<PolygonPoint> polygonPoints, ChunkMeshOptimized chunkMesh, ClipperPolygon clipperPolygon, List<ClipperPolygon> clipperPolygonHoles) : base(polygonPoints)
            {
                _chunkMesh = chunkMesh;

                if(clipperPolygonHoles != null)
                {
                    foreach(ClipperPolygon clipperPolygonHole in clipperPolygonHoles)
                    {
                        if(_IsHoleInPolygon(clipperPolygon, clipperPolygonHole))
                        {
                            AddHole(Factory(chunkMesh, clipperPolygonHole, null));
                        }
                    }
                }
            }

            public void Process(Vector3 position, Vector3 normal, Color color)
            {
                P2T.Triangulate(this);

                foreach (DelaunayTriangle triangle in this.Triangles)
                {
                    List<TriangulationPoint> points = triangle.Points.ToList();

                    if (normal == Vector3.Right || normal == Vector3.Forward || normal == Vector3.Down)
                        points = triangle.Points.Reverse().ToList();

                    foreach (TriangulationPoint point in points)
                    {
                        VertexPositionColorNormal vertex = new VertexPositionColorNormal(position + _PolygonPointToVector3(point, normal), color, normal);
                        int vertexIndex;

                        vertexIndex = _chunkMesh._verticesToIndices.TryGetValue(vertex, out vertexIndex) ? vertexIndex : -1;
                        if (vertexIndex == -1)
                        {
                            vertexIndex = _chunkMesh._globalVertexIndex;
                            _chunkMesh._verticesToIndices.Add(vertex, vertexIndex);
                            _chunkMesh._globalVertexIndex++;
                        }
                        
                        _chunkMesh.Indices.Add(vertexIndex);
                    }
                }
            }

            private bool _IsHoleInPolygon(ClipperPolygon polygon, ClipperPolygon hole)
            {
                bool hasOutsideOrEdgePoint = false;

                foreach(IntPoint point in hole)
                {
                    if(Clipper.PointInPolygon(point, polygon) != 1)
                    {
                        hasOutsideOrEdgePoint = true;
                        break;
                    }
                }

                return !hasOutsideOrEdgePoint;
            }

            private Vector3 _PolygonPointToVector3(TriangulationPoint point, Vector3 normal)
            {
                Vector3 result;

                if (normal == Vector3.Backward || normal == Vector3.Forward)
                    result = new Vector3(point.Xf, point.Yf, 0);
                else if (normal == Vector3.Down || normal == Vector3.Up)
                    result = new Vector3(point.Xf, 0, point.Yf);
                else if (normal == Vector3.Left || normal == Vector3.Right)
                    result = new Vector3(0, point.Xf, point.Yf);
                else
                    throw new Exception("Bad normal vector3 given");

                return result;
            }
        }
        
        public ChunkMeshOptimized(Chunk chunk) 
            : base(chunk)
        {
            _verticesToIndices = new Dictionary<VertexPositionColorNormal, int>();
            _globalVertexIndex = 0;
        }

        public override void Build()
        {
            Clear();

            int chunkWidth = Chunk.Width,
                chunkHeight = Chunk.Height,
                chunkDepth = Chunk.Depth;

            int[, , ,] facesMapping = new int[chunkWidth, chunkHeight, chunkDepth, 6];

            for (int x = 0; x < chunkWidth; x++)
            {
                for (int y = 0; y < chunkHeight; y++)
                {
                    for (int z = 0; z < chunkDepth; z++)
                    {
                        int[] blockFacesMapping = _GetBlockMeshFacesMapping(Chunk.Blocks[x][y][z], x, y, z);

                        for (int i = 0; i < 6; i++)
                        {
                            facesMapping[x, y, z, i] = blockFacesMapping[i];
                        }
                    }
                }
            }

            // Backward and Forward faces
            for (int i = 0; i < 2; i++)
            {
                for (int z = 0; z < chunkDepth; z++)
                {
                    int[,] toProcessFacesMapping = new int[chunkWidth, chunkHeight];
                    bool hasFaceMapped = false;

                    for (int x = 0; x < chunkWidth; x++)
                    {
                        for (int y = 0; y < chunkHeight; y++)
                        {
                            int value = facesMapping[x, y, z, i];

                            if (value > 0)
                            {
                                toProcessFacesMapping[x, y] = value;
                                hasFaceMapped = true;
                            }
                        }
                    }

                    if (hasFaceMapped)
                    {
                        _ProcessPolygonsTriangulation(toProcessFacesMapping, Chunk.BoundingBox.Min + new Vector3(0, 0, z + i), i != 0 ? Vector3.Forward : Vector3.Backward);
                    }
                }
            }

            // Left and Right faces
            for (int i = 2; i < 4; i++)
            {
                for (int x = 0; x < chunkWidth; x++)
                {
                    int[,] toProcessFacesMapping = new int[chunkHeight, chunkDepth];
                    bool hasFaceMapped = false;

                    for (int y = 0; y < chunkHeight; y++)
                    {
                        for (int z = 0; z < chunkDepth; z++)
                        {
                            int value = facesMapping[x, y, z, i];

                            if (value > 0)
                            {
                                toProcessFacesMapping[y, z] = value;
                                hasFaceMapped = true;
                            }
                        }
                    }

                    if (hasFaceMapped)
                    {
                        _ProcessPolygonsTriangulation(toProcessFacesMapping, Chunk.BoundingBox.Min + new Vector3(x + i - 2, 0, 0), i != 2 ? Vector3.Right : Vector3.Left);
                    }
                }
            }
            
            // Down and Up faces
            for(int i = 4; i < 6; i++)
            {
                for(int y = 0; y < chunkHeight; y ++)
                {
                    int[,] toProcessFacesMapping = new int[chunkWidth, chunkDepth];
                    bool hasFaceMapped = false;

                    for(int x = 0; x < chunkWidth; x ++)
                    {
                        for (int z = 0; z < chunkDepth; z++)
                        {
                            int value = facesMapping[x, y, z, i];

                            if (value > 0)
                            {
                                toProcessFacesMapping[x, z] = value;
                                hasFaceMapped = true;
                            }
                        }
                    }

                    if (hasFaceMapped)
                    {
                        _ProcessPolygonsTriangulation(toProcessFacesMapping, Chunk.BoundingBox.Min + new Vector3(0, y + i - 4, 0), i != 4 ? Vector3.Up : Vector3.Down);
                    }
                }
            }

            Vertices = _verticesToIndices.OrderBy(kvp => kvp.Value).Select(kvp => kvp.Key).ToList<VertexPositionColorNormal>();

            _verticesToIndices.Clear();
        }
        
        private int[] _GetBlockMeshFacesMapping(Block block, int inChunkX, int inChunkY, int inChunkZ)
        {
            if (block.IsAir)
                return new int[6] { 0, 0, 0, 0, 0, 0 };

            Vector3 worldPosition = Chunk.BoundingBox.Min + new Vector3(inChunkX, inChunkY, inChunkZ);

            int[] faces = new int[6];

            // Backward face 
            faces[0] = Chunk.ChunkManager.GetBlockAtPosition(worldPosition + Vector3.Forward).IsTransparent ? 1 : 0;

            // Forward face
            faces[1] = Chunk.ChunkManager.GetBlockAtPosition(worldPosition + Vector3.Backward).IsTransparent ? 1 : 0;

            // Left face
            faces[2] = Chunk.ChunkManager.GetBlockAtPosition(worldPosition + Vector3.Left).IsTransparent ? 1 : 0;

            // Right face
            faces[3] = Chunk.ChunkManager.GetBlockAtPosition(worldPosition + Vector3.Right).IsTransparent ? 1 : 0;

            // Down Face
            faces[4] = Chunk.ChunkManager.GetBlockAtPosition(worldPosition + Vector3.Down).IsTransparent ? 1 : 0;

            // Up face
            faces[5] = Chunk.ChunkManager.GetBlockAtPosition(worldPosition + Vector3.Up).IsTransparent ? 1 : 0;

            return faces;
        }
        
        private void _ProcessPolygonsTriangulation(int[,] facesMapping, Vector3 position, Vector3 normal)
        {
            int max1 = facesMapping.GetUpperBound(0) + 1;
            int max2 = facesMapping.GetUpperBound(1) + 1;

            List<ClipperPolygon> subjects = new List<ClipperPolygon>();
            List<ClipperPolygon> clips = new List<ClipperPolygon>();
            List<ClipperPolygon> solution = new List<ClipperPolygon>();

            Clipper cp = new Clipper();
            
            for (int i = 0; i < max1; i++)
            {
                for (int j = 0; j < max2; j++)
                {
                    int value = facesMapping[i, j];

                    if (value != 0)
                    {
                        ClipperPolygon polygon = new ClipperPolygon();
                        polygon.Add(new IntPoint(i, j));
                        polygon.Add(new IntPoint(i + 1, j));
                        polygon.Add(new IntPoint(i + 1, j + 1));
                        polygon.Add(new IntPoint(i, j + 1));
                        cp.AddPath(polygon, PolyType.ptSubject, true);
                    }
                }
            }

            cp.Execute(ClipType.ctUnion, solution, PolyFillType.pftNonZero, PolyFillType.pftNonZero);

            List<ClipperPolygon> polygons = solution.FindAll(delegate(ClipperPolygon path)
            {
                return Clipper.Orientation(path);
            });

            List<ClipperPolygon> holes = solution.Except(polygons).ToList();
            
            Color c = Color.Gray;
                
            foreach(List<IntPoint> polygon in polygons)
            {
                Poly2TriPolygonFromClipper poly2triPolygon = Poly2TriPolygonFromClipper.Factory(this, polygon, holes);
                poly2triPolygon.Process(position, normal, c);
            }
        }
    }
}
