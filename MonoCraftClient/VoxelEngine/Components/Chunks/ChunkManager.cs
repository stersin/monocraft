﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoCraftClient.VoxelEngine.Components.Blocks;
using MonoCraftClient.VoxelEngine.Lib.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MonoCraftClient.VoxelEngine.Components.Chunks
{
    public class ChunkManager : WorldDrawableComponent
    {
        #region camera management properties

        private Chunk _lastRefChunk;
        private Vector3 _lastCameraPosition;
        private Matrix _lastCameraView;

        public ICamera Camera { get; set; }
        
        #endregion

        #region chunks management properties

        private Dictionary<int, Chunk> _chunksDict;
        private List<Chunk> _loadChunksList;
        private List<Chunk> _setupChunksList;
        private List<Chunk> _rebuildMeshChunksList;
        private List<Chunk> _unloadChunksList;
        private List<Chunk> _visibilityChunksList;
        private List<Chunk> _renderChunksList;

        #endregion

        #region rendering proterties

        private Boolean _rebuildVisibilityList;
        private RasterizerState _rasterizedState;
        public BasicEffect Effect { get; private set; }
        public SpriteBatch SpriteBatch { get; private set; }
        public FillMode FillMode
        {
            get
            {
                return _rasterizedState.FillMode;
            }
            set
            {
                _rasterizedState.FillMode = value;
            }
        }
        
        #endregion

        #region debug proterties

        private Object _debugDataLock = new Object();
        public int DebugLoadedChunks { get; private set; }
        public int DebugSetupChunks { get; private set; }
        public int DebugRenderedChunks { get; private set; }
        public int DebugLoadedBlocks { get; private set; }
        public int DebugLoadedVertices { get; private set; }
        public int DebugRenderedVertices { get; private set; }
        public Vector3 DebugCameraChunkOffset { get { return _lastRefChunk != null ? _lastRefChunk.Offset : Vector3.Zero; } }
        
        #endregion 
        
        public ChunkManager(Game game, World world, ICamera camera)
            : base(game, world)
        {
            _chunksDict = new Dictionary<int, Chunk>();
            _loadChunksList = new List<Chunk>();
            _setupChunksList = new List<Chunk>();
            _rebuildMeshChunksList = new List<Chunk>();
            _unloadChunksList = new List<Chunk>();
            _visibilityChunksList = new List<Chunk>();
            _renderChunksList = new List<Chunk>();
            
            Camera = camera;

            Effect = new BasicEffect(GraphicsDevice);
            Effect.World = Matrix.Identity;
            Effect.VertexColorEnabled = true;

            _rasterizedState = new RasterizerState()
            {
                CullMode = CullMode.CullCounterClockwiseFace,
                FillMode = FillMode.WireFrame
            };

            SpriteBatch = new SpriteBatch(GraphicsDevice);

            DebugLoadedChunks = DebugSetupChunks = DebugRenderedChunks = DebugLoadedBlocks = DebugLoadedVertices = DebugRenderedVertices = 0;

            ThreadPool.SetMaxThreads(8, 8);

        }

        #region chunks and blocks accessors
        public bool IsValidPosition(int x, int y, int z)
        {
            return x >= 0 && x < World.WorldConf.ChunkWidth * World.WorldConf.NumChunksWidth
                && y >= 0 && y < World.WorldConf.ChunkHeight * World.WorldConf.NumChunksHeight
                && z >= 0 && z < World.WorldConf.ChunkDepth * World.WorldConf.NumChunksDepth;
        }

        public bool IsValidPosition(Vector3 position)
        {
            return position.X >= 0 && position.X < World.WorldConf.ChunkWidth * World.WorldConf.NumChunksWidth
                && position.Y >= 0 && position.Y < World.WorldConf.ChunkHeight * World.WorldConf.NumChunksHeight
                && position.Z >= 0 && position.Z < World.WorldConf.ChunkDepth * World.WorldConf.NumChunksDepth;
        }

        public bool IsValidChunkOffset(int offsetX, int offsetY, int offsetZ)
        {
            return offsetX >= 0 && offsetX < World.WorldConf.NumChunksWidth
                && offsetY >= 0 && offsetY < World.WorldConf.NumChunksHeight
                && offsetZ >= 0 && offsetZ < World.WorldConf.NumChunksDepth;
        }

        public bool IsValidChunkOffset(Vector3 offset)
        {
            return offset.X >= 0 && offset.X < World.WorldConf.NumChunksWidth
                && offset.Y >= 0 && offset.Y < World.WorldConf.NumChunksHeight
                && offset.Z >= 0 && offset.Z < World.WorldConf.NumChunksDepth;
        }

        public Chunk GetChunk(int offsetX, int offsetY, int offsetZ, bool createIfNotExisting = false)
        {
            Chunk foundChunk = null;

            if (IsValidChunkOffset(offsetX, offsetY, offsetZ))
            {
                int index = offsetX + World.WorldConf.NumChunksWidth * (offsetY + World.WorldConf.NumChunksDepth * offsetZ);

                foundChunk = _chunksDict.TryGetValue(index, out foundChunk) ? foundChunk : null;

                if (foundChunk == null && createIfNotExisting)
                {
                    Vector3 chunkMin = new Vector3(offsetX * World.WorldConf.ChunkWidth, offsetY * World.WorldConf.ChunkHeight, offsetZ * World.WorldConf.ChunkDepth);
                    Vector3 chunkMax = chunkMin + new Vector3(World.WorldConf.ChunkWidth, World.WorldConf.ChunkHeight, World.WorldConf.ChunkDepth);

                    foundChunk = new Chunk(Game, World, this,
                        new Vector3(offsetX, offsetY, offsetZ),
                        new BoundingBox(chunkMin, chunkMax));

                    _chunksDict.Add(index, foundChunk);
                }
            }

            return foundChunk;
        }

        public Chunk GetChunk(Vector3 offset, bool createIfNotExisting = false)
        {
            return GetChunk((int)offset.X, (int)offset.Y, (int)offset.Z, createIfNotExisting);
        }

        public Chunk GetChunkAtPosition(Vector3 position, bool createIfNotExisting = false)
        {
            return IsValidPosition(position) ? GetChunk(
                (int)(position.X / World.WorldConf.ChunkWidth),
                (int)(position.Y / World.WorldConf.ChunkHeight),
                (int)(position.Z / World.WorldConf.ChunkDepth),
                createIfNotExisting
                ) : null;
        }

        public Chunk GetChunkAtPosition(int x, int y, int z, bool createIfNotExisting = false)
        {
            return IsValidPosition(x, y, z) ? GetChunk(
                (int)(x / World.WorldConf.ChunkWidth),
                (int)(y / World.WorldConf.ChunkHeight),
                (int)(z / World.WorldConf.ChunkDepth),
                createIfNotExisting
                ) : null;
        }

        public Block GetBlockAtPosition(int x, int y, int z)
        {
            Block block = new Block(Block.BlockType.Invalid);

            if (IsValidPosition(x, y, z))
            {
                Chunk chunk = GetChunkAtPosition(x, y, z);

                if (chunk != null && chunk.IsLoaded)
                {
                    Vector3 chunkPosition = chunk.BoundingBox.Min;
                    block = chunk.Blocks[x - (int)chunkPosition.X][y - (int)chunkPosition.Y][z - (int)chunkPosition.Z];
                }
            }

            return block;
        }

        public Block GetBlockAtPosition(Vector3 position)
        {
            return GetBlockAtPosition((int)position.X, (int)position.Y, (int)position.Z);
        }
        #endregion

        #region DrawableGameComponents overrides
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            _UpdateUnloadList();
            
            _UpdateRebuildMeshList();
            
            #region visibility list 
            
            // if actual ref chunk is different from previous, visibility list have to be rebuilt
            _UpdateRefChunk();
                

            if (_rebuildVisibilityList)
                _UpdateVisibilityList();

            #endregion

            #region render list
            Vector3 cameraPosition = Camera.Position;
            Matrix cameraView = Camera.View;

            if (_lastCameraPosition != cameraPosition || _lastCameraView != cameraView)
                _UpdateRenderList();

            _lastCameraPosition = cameraPosition;
            _lastCameraView = cameraView;
            #endregion

        }

        public override void Draw(GameTime gameTime)
        {

            GraphicsDevice.RasterizerState = _rasterizedState;

            for (short i = 0; i < _renderChunksList.Count; i++)
            {
                Chunk chunk = _renderChunksList[i];

                if (chunk.CanBeDrawn == true)
                {
                    chunk.Draw(gameTime);
                }
            }

            base.Draw(gameTime);
        }
        #endregion

        #region callbacks
        public void OnChunkLoadedCallback(Chunk chunk, int loadedBlocks)
        {
            lock(_debugDataLock)
            {
                DebugLoadedChunks++;
                DebugLoadedBlocks += loadedBlocks;
                _rebuildVisibilityList = true;
            }
        }

        public void OnChunkSetupCallback(Chunk chunk, int loadedVertices)
        {
            lock (_debugDataLock)
            {
                DebugSetupChunks++;
                DebugLoadedVertices += loadedVertices;
                _rebuildVisibilityList = true;
            }
        }

        public void OnChunkUnloadedCallback(Chunk chunk, int unloadedBlocks, int unloadedVertices)
        {
            lock (_debugDataLock)
            {
                DebugLoadedBlocks -= unloadedBlocks;
                DebugLoadedVertices -= unloadedVertices;

                if(chunk.IsSetup)
                    DebugSetupChunks--;

                DebugLoadedChunks--;

                int index = (int)chunk.Offset.X + World.WorldConf.NumChunksWidth * ((int)chunk.Offset.Y + World.WorldConf.NumChunksDepth * (int)chunk.Offset.Z);
                _chunksDict.Remove(index);
            }
        }
        #endregion

        private void _UpdateRefChunk()
        {
             Vector3 inChunkCameraPosition = Camera.Position;

             if (inChunkCameraPosition.X < 0)
                 inChunkCameraPosition.X = 0;
             else if (inChunkCameraPosition.X >= World.WorldConf.NumChunksWidth * World.WorldConf.ChunkWidth)
                 inChunkCameraPosition.X = World.WorldConf.NumChunksWidth * World.WorldConf.ChunkWidth - 1;

            if (inChunkCameraPosition.Y < 0)
                inChunkCameraPosition.Y = 0;
            else if (inChunkCameraPosition.Y >= World.WorldConf.NumChunksHeight * World.WorldConf.ChunkHeight)
                inChunkCameraPosition.Y = World.WorldConf.NumChunksHeight * World.WorldConf.ChunkHeight - 1;

            if (inChunkCameraPosition.Z < 0)
                inChunkCameraPosition.Z = 0;
            else if (inChunkCameraPosition.Z >= World.WorldConf.NumChunksDepth * World.WorldConf.ChunkDepth)
                inChunkCameraPosition.Z = World.WorldConf.NumChunksDepth * World.WorldConf.ChunkDepth - 1;

            // actual ref chunk is calculated from the camera position
            Chunk refChunk = GetChunkAtPosition(inChunkCameraPosition, true);

            if (refChunk != _lastRefChunk)
            {
                _lastRefChunk = refChunk;
                _rebuildVisibilityList = true;
            }
        }

        #region lists processing
        private void _UpdateUnloadList()
        {
            for (int i = 0; i < _unloadChunksList.Count; i++)
            {
                Chunk chunk = _unloadChunksList[i];

                chunk.Unload(OnChunkUnloadedCallback);
            }

            _unloadChunksList.Clear();
        }
        
        private void _UpdateRebuildMeshList()
        {
            for (int i = 0; i < _rebuildMeshChunksList.Count; i++)
            {
                Chunk chunk = _rebuildMeshChunksList[i];

                if (chunk.IsLoaded && chunk.IsSetup)
                {
                    chunk.RebuildMesh();

                    _rebuildVisibilityList = true;
                }
            }

            _rebuildMeshChunksList.Clear();
        }

        private void _UpdateVisibilityList()
        {
            List<Chunk> newVisibilyChunksList = new List<Chunk>();

            if (_lastRefChunk != null)
            {
                _addToVisibilityList(newVisibilyChunksList, (int)_lastRefChunk.Offset.X, (int)_lastRefChunk.Offset.Y, (int)_lastRefChunk.Offset.Z, true);
                
                int centerX = (int)_lastRefChunk.Offset.X;
                int centerY = (int)_lastRefChunk.Offset.Y;
                int centerZ = (int)_lastRefChunk.Offset.Z;

                for (int i = 1; i <= World.WorldConf.ChunksToLoadRadius; i++)
                {
                    bool willBeRendered = i <= World.WorldConf.ChunksToLoadRadius;

                    for (int x = -i; x <= i; x += 2 * i)
                        for (int y = -i; y <= i; y++)
                            for (int z = -i; z <= i; z++)
                            {
                                _addToVisibilityList(newVisibilyChunksList, centerX + x, centerY + y, centerZ + z, willBeRendered);
                            }

                    for (int y = -i; y <= i; y += 2 * i)
                        for (int x = -i + 1; x <= i - 1; x++)
                            for (int z = -i; z <= i; z++)
                            {
                                _addToVisibilityList(newVisibilyChunksList, centerX + x, centerY + y, centerZ + z, willBeRendered);
                            }

                    for (int z = -i; z <= i; z += 2 * i)
                        for (int y = -i + 1; y <= i - 1; y++)
                            for (int x = -i + 1; x <= i - 1; x++)
                            {
                                _addToVisibilityList(newVisibilyChunksList, centerX + x, centerY + y, centerZ + z, willBeRendered);
                            }
                }
            }

            // unload chunk list is done by diffing the old visibility list with the new one
            _unloadChunksList = _visibilityChunksList.Except(newVisibilyChunksList).ToList();
            _visibilityChunksList = newVisibilyChunksList;

            _rebuildVisibilityList = false;
        }

        private void _addToVisibilityList(List<Chunk> newVisibilyChunksList, int x, int y, int z, bool willBeRendered)
        {
            Chunk chunk = GetChunk(x, y, z, true);

            if (chunk != null)
            {
                newVisibilyChunksList.Add(chunk);

                if (!chunk.IsLoaded)
                {
                    chunk.Load(OnChunkLoadedCallback);
                }
                else if (willBeRendered && !chunk.IsSetup)
                {
                    chunk.Setup(OnChunkSetupCallback);
                }
            }
        }

        private void _UpdateRenderList()
        {
            _renderChunksList.Clear();
            
            DebugRenderedChunks = DebugRenderedVertices = 0;

            for (int i = 0; i < _visibilityChunksList.Count; i++)
            {
                Chunk chunk = _visibilityChunksList[i];

                if (chunk.IsLoaded && chunk.IsSetup && chunk.IsEmpty == false && Camera.InViewFrustrum(chunk.BoundingBox))
                {
                    _renderChunksList.Add(chunk);

                    DebugRenderedChunks++;

                    DebugRenderedVertices += chunk.DebugRenderedVertices;
                }
            }
        }
        #endregion

    }
}
