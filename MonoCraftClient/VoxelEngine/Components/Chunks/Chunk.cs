﻿using Graphics.Tools.Noise;
using Graphics.Tools.Noise.Builder;
using Graphics.Tools.Noise.Filter;
using Graphics.Tools.Noise.Primitive;
using Graphics.Tools.Noise.Renderer;
using Graphics.Tools.Noise.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoCraftClient.VoxelEngine.Components.Blocks;
using MonoCraftClient.VoxelEngine.Lib;
using MonoCraftClient.VoxelEngine.Lib.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MonoCraftClient.VoxelEngine.Components.Chunks
{
    public class Chunk : WorldDrawableComponent
    {
        public ChunkManager ChunkManager { get; private set; }
        public ChunkMesh ChunkMesh { get; private set; }
        public Vector3 Offset { get; private set; }

        public int Width { get { return (int)(BoundingBox.Max.X - BoundingBox.Min.X); } }
        public int Height { get { return (int)(BoundingBox.Max.Y - BoundingBox.Min.Y); } }
        public int Depth { get { return (int)(BoundingBox.Max.Z - BoundingBox.Min.Z); } }

        public BoundingBox BoundingBox { get; private set; }

        public Boolean IsEmpty { get; private set; }

        public Boolean BeingLoaded { get; private set; }
        public Boolean IsLoaded { get; private set; }
        public Boolean CanBeLoaded { get { return BeingLoaded == false && IsLoaded == false; } }

        public Boolean BeingSetup { get; private set; }
        public Boolean IsSetup { get; private set; }
        public Boolean CanBeSetup { get { return _checkCanBeSetup(); } }

        public Boolean BeingUnloaded { get; private set; }
        public Boolean CanBeUnloaded { get { return BeingUnloaded == false && IsLoaded == true && BeingSetup == false; } }

        public Boolean CanBeDrawn { get { return IsSetup == true && IsEmpty == false; } }

        private VertexBuffer _meshVertexBuffer;
        private IndexBuffer _meshIndexBuffer;

        public Block[][][] Blocks;
        
        #region callbacks
        public delegate void OnLoadedCallback(Chunk chunk, int loadedBlocks);
        public delegate void OnSetupCallback(Chunk chunk, int loadedVertices);
        public delegate void OnUnloadedCallback(Chunk chunk, int unloadedBlocks, int unloadedVertices);
        #endregion  

        #region debug proterties
        public int DebugLoadedBlocks { get; private set; }
        public int DebugLoadedVertices { get; private set; }
        public int DebugRenderedVertices { get; private set; }
        private bool _debugLoadDebugShape = false;
        #endregion 

        public Chunk(Game game, World world, ChunkManager chunkManager, Vector3 offset, BoundingBox boundingBox) : base(game, world)
        {
            ChunkManager = chunkManager;
            ChunkMesh = new ChunkMeshOptimized(this);
            Offset = offset;
            BoundingBox = boundingBox;
            IsEmpty = true;
            IsLoaded = false;
            IsSetup = false;

            DebugLoadedBlocks = 0;
            DebugLoadedVertices = 0;
            DebugRenderedVertices = 0;
        }

        #region DrawableGameComponents overrides
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            if (_meshVertexBuffer != null && _meshVertexBuffer.VertexCount > 0)
            {
                BasicEffect effect = ChunkManager.Effect;
                effect.World = Matrix.Identity;
                effect.View = ChunkManager.Camera.View;
                effect.Projection = ChunkManager.Camera.Projection;
                effect.EnableDefaultLighting();

                foreach (EffectPass pass in effect.CurrentTechnique.Passes)
                {
                    pass.Apply();

                    GraphicsDevice.SetVertexBuffer(_meshVertexBuffer);
                    GraphicsDevice.Indices = _meshIndexBuffer;
                    GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, _meshVertexBuffer.VertexCount, 0, _meshIndexBuffer.IndexCount / 3);
                }
            }

            base.Draw(gameTime);
        }
        #endregion

        public void Load(OnLoadedCallback callback)
        {
            if (CanBeLoaded)
            {
                BeingLoaded = true;
                ThreadPool.QueueUserWorkItem(_loadThread, callback);
            }
        }

        private void _loadThread(Object threadContext)
        {
            OnLoadedCallback callback = (OnLoadedCallback)threadContext;

            Blocks = new Block[Width][][];

            for (short x = 0; x < Width; x++)
            {
                Blocks[x] = new Block[Height][];

                for (short y = 0; y < Height; y++)
                {
                    Blocks[x][y] = new Block[Depth];
                }
            }

            if (_debugLoadDebugShape)
                _LoadDebugShape();
            else
                _LoadNoiseShape();

            DebugLoadedBlocks = Width * Height * Depth;

            IsLoaded = true;
            BeingLoaded = false;

            callback(this, DebugLoadedBlocks);
        }

        public void Setup(OnSetupCallback callback)
        {
            if (_checkCanBeSetup())
            {
                BeingSetup = true;
                ThreadPool.QueueUserWorkItem(_setupThread, callback);
            }
        }

        private void _setupThread(Object threadContext)
        {
            OnSetupCallback callback = (OnSetupCallback)threadContext;

            RebuildMesh();

            IsSetup = true;

            BeingSetup = false;

            callback(this, DebugLoadedVertices);
        }

        private bool _checkCanBeSetup()
        {
            // chunk state checks
            if (BeingSetup == true || IsLoaded == false || BeingUnloaded == true || IsEmpty == true)
                return false;

            Chunk[] chunksToCheck = new Chunk[6];

            Vector3[] directions = new Vector3[6] { Vector3.Forward, Vector3.Backward, Vector3.Left, Vector3.Right, Vector3.Up, Vector3.Down };

            for (int i = 0; i < 6; i++)
            {
                Vector3 chunkOffset = Offset + directions[i];

                // if chunk offset is invalid, it's an out of world chunk, so we continue our check
                if (!ChunkManager.IsValidChunkOffset(chunkOffset))
                    continue;

                Chunk chunk = ChunkManager.GetChunk(chunkOffset);

                if (chunk == null || chunk.IsLoaded == false)
                    return false;
            }

            return true;
        }

        public void Unload(OnUnloadedCallback callback)
        {
            if (CanBeUnloaded)
            {
                BeingUnloaded = true;
                ThreadPool.QueueUserWorkItem(_unloadThread, callback);
            }

        }

        private bool _checkCanBeUnloaded()
        {
            // chunk state checks
            if (IsLoaded == true && BeingSetup == false)
                return false;

            return true;
        }

        private void _unloadThread(Object threadContext)
        {
            OnUnloadedCallback callback = (OnUnloadedCallback)threadContext;

            IsEmpty = true;
            IsLoaded = false;
            IsSetup = false;
            Blocks = null;

            if (_meshIndexBuffer != null)
                _meshIndexBuffer.Dispose();

            if (_meshVertexBuffer != null)
                _meshVertexBuffer.Dispose();

            int unloadedBlocks = DebugLoadedBlocks;
            int unloadedVertices = DebugLoadedVertices;

            DebugLoadedBlocks = DebugLoadedVertices = DebugRenderedVertices = 0;

            BeingUnloaded = false;

            callback(this, unloadedBlocks, unloadedVertices);
        }

        public void RebuildMesh()
        {
            ChunkMesh.Build();

            DebugLoadedVertices = DebugRenderedVertices = ChunkMesh.Vertices.Count;

            if (ChunkMesh.Vertices.Count > 0)
            {
                _meshVertexBuffer = new VertexBuffer(GraphicsDevice, typeof(VertexPositionColorNormal), ChunkMesh.Vertices.Count, BufferUsage.WriteOnly);
                _meshVertexBuffer.SetData(ChunkMesh.Vertices.ToArray());
                                
                _meshIndexBuffer = new IndexBuffer(GraphicsDevice, typeof(int), ChunkMesh.Indices.Count, BufferUsage.WriteOnly);
                _meshIndexBuffer.SetData(ChunkMesh.Indices.ToArray());
            }

            ChunkMesh.Clear();
        }

        private void _LoadDebugShape()
        {
            Vector3 chunkPosition = BoundingBox.Min;

            int worldHeight = World.WorldConf.NumChunksHeight * Height;

            int worldY = (int)chunkPosition.Y;
            Random i = new Random();

            for (int x = 0; x < Width; x++)
            {
                for (int z = 0; z < Depth; z++)
                {
                    int worldX = (int)chunkPosition.X + x;
                    int worldZ = (int)chunkPosition.Z + z;

                    float rnd = 0.5f;

                    int groundHeight = (int)Math.Floor(rnd * worldHeight) - worldY;

                    groundHeight += x % 8 >= 4 && z % 8 >= 4 ? 1 : 0;
                                        
                    int curY = 0;

                    for (; curY < groundHeight && curY < Height; curY++)
                        Blocks[x][curY][z] = new Block(Block.BlockType.Dirt);

                    if (curY == groundHeight && curY < Height)
                    {
                        Blocks[x][curY][z] = new Block(Block.BlockType.Grass);
                        curY++;
                    }

                    if (curY > 0)
                        IsEmpty = false;

                    for (; curY < Height; curY++)
                    {
                        Blocks[x][curY][z] = new Block(Block.BlockType.Air);
                    }
                }
            }
        }

        private void _LoadNoiseShape()
        {
            Vector3 chunkPosition = BoundingBox.Min;
            int worldY = (int)chunkPosition.Y;

            int maxGroundDelta = World.WorldConf.WorldMaxGroundHeight - World.WorldConf.WorldMinGroundHeight;
            
            Heightmap32 heightMap = new Heightmap32();
            NoiseMap noiseMap = new NoiseMap(Width, Depth);

            NoiseMapBuilderPlane heightMapBuilder = new NoiseMapBuilderPlane(BoundingBox.Min.X, BoundingBox.Max.X, BoundingBox.Min.Z, BoundingBox.Max.Z, false);

            SimplexPerlin noiseModule = new SimplexPerlin(1, Graphics.Tools.Noise.NoiseQuality.Best);
            
            SumFractal filterModule = new SumFractal();
            filterModule.Frequency = 0.005f;
            filterModule.Frequency = 0.003f;
            filterModule.Lacunarity = 2;
            filterModule.OctaveCount = 6;
            filterModule.Offset = 1;
            filterModule.Gain = 2;
            filterModule.Primitive3D = (IModule3D)noiseModule;

            heightMapBuilder.SourceModule = filterModule;
            heightMapBuilder.NoiseMap = noiseMap;
            heightMapBuilder.SetSize(Width, Depth);
            heightMapBuilder.Build();

            Heightmap32Renderer heightMapRenderer = new Heightmap32Renderer();

            heightMapRenderer.NoiseMap = noiseMap;
            heightMapRenderer.Heightmap = heightMap;
            heightMapRenderer.SetBounds(-1, 1);
            heightMapRenderer.Render();

            for (int x = 0; x < Width; x++)
            {
                for (int z = 0; z < Depth; z++)
                {
                    int worldX = (int)chunkPosition.X + x;
                    int worldZ = (int)chunkPosition.Z + z;

                    float groundHeightPct = (heightMap.GetValue(x, z) + 1) / 2;

                    int groundHeight = (int)(World.WorldConf.WorldMinGroundHeight + maxGroundDelta * groundHeightPct);
                                        
                    int dirtHeight = groundHeight - 4;

                    int curY = 0;

                    for (; worldY + curY < (dirtHeight) && curY < Height; curY++)
                        Blocks[x][curY][z] = new Block(Block.BlockType.Rock);

                    for (; worldY + curY < groundHeight && curY < Height; curY++)
                        Blocks[x][curY][z] = new Block(Block.BlockType.Dirt);

                    if (worldY + curY == groundHeight && curY < Height)
                    {
                        Blocks[x][curY][z] = new Block(Block.BlockType.Grass);
                        curY++;
                    }

                    /*for (; curY < Map.WATER_HEIGHT * NUM_CHUNKS_HEIGHT * MapChunk.HEIGHT; curY++)
                    {
                        Blocks[x][curY][z] = new Block(BlockType.Water);
                    }*/

                    if (curY > 0)
                        IsEmpty = false;

                    // fill remaining blocks with air blocks
                    for (;curY < Height; curY++)
                    {
                        Blocks[x][curY][z] = new Block(Block.BlockType.Air);
                    }
                }
            }
        }

    }
}
