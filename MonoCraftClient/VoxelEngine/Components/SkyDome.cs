using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using MonoCraftClient.VoxelEngine.Lib.Components;


namespace MonoCraftClient.VoxelEngine.Components
{
    public class SkyDome : WorldDrawableComponent
    {
        #region Properties

        private Model _skyDome;
        private Texture2D _cloudMap;

        #endregion

        #region GameComponent

        public SkyDome(Game game, World world)
            : base(game, world)
        {
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            //_effect = _game.Content.Load<Effect>("Effects/skyEffect");
            _cloudMap = Game.Content.Load<Texture2D>("Textures/cloudMap");
            _skyDome = Game.Content.Load<Model>("Models/skyDome");
            //_skyDome.Meshes[0].MeshParts[0].Effect = _effect;

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            var rs = new RasterizerState();
            rs.CullMode = CullMode.CullCounterClockwiseFace;
            GraphicsDevice.RasterizerState = rs;

            Matrix[] modelTransforms = new Matrix[_skyDome.Bones.Count];
            _skyDome.CopyAbsoluteBoneTransformsTo(modelTransforms);

            /*Matrix wMatrix = Matrix.CreateTranslation(0, -0.3f, 0) * Matrix.CreateScale(100) * Matrix.CreateTranslation(_game.Camera.Position);
            foreach (ModelMesh mesh in _skyDome.Meshes)
            {
                foreach (Effect currentEffect in mesh.Effects)
                {
                    Matrix worldMatrix = modelTransforms[mesh.ParentBone.Index] * wMatrix;
                    currentEffect.CurrentTechnique = currentEffect.Techniques["Textured"];
                    currentEffect.Parameters["xWorld"].SetValue(worldMatrix);
                    currentEffect.Parameters["xView"].SetValue(_game.Camera.View);
                    currentEffect.Parameters["xProjection"].SetValue(_game.Camera.Projection);
                    currentEffect.Parameters["xTexture"].SetValue(_cloudMap);
                    currentEffect.Parameters["xEnableLighting"].SetValue(false);
                }
                mesh.Draw();
            }
            */
            rs = new RasterizerState();
            rs.CullMode = CullMode.CullClockwiseFace;
            GraphicsDevice.RasterizerState = rs;

            base.Draw(gameTime);
        }

        #endregion
    }
}
