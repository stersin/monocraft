﻿using Microsoft.Xna.Framework;
using MonoCraftClient.VoxelEngine.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoCraftClient.VoxelEngine.Lib.Components
{
    abstract public class WorldDrawableComponent : DrawableGameComponent
    {
        #region Properties

        public World World { get; private set; } 
        
        #endregion

        public WorldDrawableComponent(Game game, World world)
            : base(game)
        {
            World = world;
        }
    }
}
