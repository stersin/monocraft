﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoCraftClient.VoxelEngine.Lib.Components
{
    public interface ICamera
    {
        #region Properties

        Vector3 Position { get; set; }
        Quaternion Rotation { get; set; }
        Matrix Projection { get; set; }
        Matrix View { get; } 
        
        #endregion

        bool InViewFrustrum(BoundingBox boundingBox);

        void Rotate(Vector3 axis, float fDegrees);

        void Translate(Vector3 distance);

        void Translate(Vector3 axis, float fDistance);
    }
}
