﻿using Microsoft.Xna.Framework;
using MonoCraftClient.VoxelEngine.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoCraftClient.VoxelEngine.Lib.Components
{
    abstract public class WorldComponent : GameComponent
    {
        #region Properties

        public World World { get; private set; } 
        
        #endregion

        public WorldComponent(Game game, World world)
            : base(game)
        {
            World = world;
        }
    }
}
