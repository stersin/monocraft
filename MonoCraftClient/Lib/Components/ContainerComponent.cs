﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoCraftClient.Lib.Components
{
    abstract public class ContainerComponent : DrawableGameComponent
    {
        #region Fields

        protected List<GameComponent> components;

        #endregion

        #region GameComponent

        public ContainerComponent(Game game)
            : base(game)
        {
            components = new List<GameComponent>();
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            foreach(GameComponent component in components)
            {
                component.Initialize();
            }

            base.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();
        }


        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            foreach(GameComponent component in components)
            {
                component.Update(gameTime);
            }
        }

        /// <summary>
        /// Called when the DrawableGameComponent needs to be drawn. Override this method
        //  with component-specific drawing code.
        /// </summary>
        /// <param name="gameTime">Time passed since the last call to Draw.</param>
        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            foreach (GameComponent component in components)
            {
                if (component is DrawableGameComponent)
                {
                    ((DrawableGameComponent)component).Draw(gameTime);
                }
            }
        }

        #endregion
    }
}
