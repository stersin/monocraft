﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MonoCraftCommonLib;
using MonoCraftCommonLib.Net;

namespace MonoCraftServerLib
{
    public class Server
    {
        enum States : short { Starting, Running, Stopping, Stopped };

        private short state;

        private UdpNetworkServer udpServer;
        private TcpNetworkServer tcpServer;

        public LoopCallback loopCallback;

        public LogCallback logCallback;

        public Server()
        {
            state = (short)States.Stopped;
        }

        #region actions 

        public void Start()
        {
            if (state != (short)States.Stopped)
            {
                throw new Exception("Cannot start server while not in Stopped state");
            }

            state = (short)States.Starting;
            WriteLogLine("Server::State => Starting ...");

            WriteLogLine("Server::Loading world ...");

            World world = new World("My world", 1);

            StartNetworking();

            state = (short)States.Running;
            WriteLogLine("Server::State => Running");

            MainLoop();

            StopNetworking();

            state = (short)States.Stopped;
            WriteLogLine("Server::State => Stopped");
        }

        public void Stop()
        {
            if (state != (short)States.Running)
            {
                throw new Exception("Cannot stop server while not in Running state");
            }

            state = (short)States.Stopping;
            WriteLogLine("Server::State => Stopping ...");
        }

        #endregion

        #region callbacks

        public delegate void LoopCallback();

        public delegate void LogCallback(String text);

        #endregion

        private void StartNetworking()
        {
            tcpServer = new TcpNetworkServer("127.0.0.1", 17070);

            tcpServer.OnServerStarting += new EventHandler<EventArgs>(delegate(object sender, EventArgs args) { WriteLogLine("[TCP Server] Server starting"); });
            tcpServer.OnServerStarted += new EventHandler<EventArgs>(delegate(object sender, EventArgs args) { WriteLogLine("[TCP Server] Server started"); });
            tcpServer.OnServerStopping += new EventHandler<EventArgs>(delegate(object sender, EventArgs args) { WriteLogLine("[TCP Server] Server stopping"); });
            tcpServer.OnServerStopped += new EventHandler<EventArgs>(delegate(object sender, EventArgs args) { WriteLogLine("[TCP Server] Server stopped"); });
            tcpServer.OnClientConnected += new EventHandler<EventArgs>(delegate(object sender, EventArgs args) { WriteLogLine("[TCP Server] Client connected"); });
            tcpServer.OnClientDisconnecting += new EventHandler<EventArgs>(delegate(object sender, EventArgs args) { WriteLogLine("[TCP Server] Client disconnecting"); });
            tcpServer.OnClientDisconnected += new EventHandler<EventArgs>(delegate(object sender, EventArgs args) { WriteLogLine("[TCP Server] Client disconnected"); });
            tcpServer.OnDataReceived += new EventHandler<EventArgs>(delegate(object sender, EventArgs args) { WriteLogLine("[TCP Server] Data received"); });
            
            udpServer = new UdpNetworkServer("127.0.0.1", 17071);

            udpServer.OnServerStarting += new EventHandler<EventArgs>(delegate(object sender, EventArgs args) { WriteLogLine("[UDP Server] Server starting"); });
            udpServer.OnServerStarted += new EventHandler<EventArgs>(delegate(object sender, EventArgs args) { WriteLogLine("[UDP Server] Server started"); });
            udpServer.OnServerStopping += new EventHandler<EventArgs>(delegate(object sender, EventArgs args) { WriteLogLine("[UDP Server] Server stopping"); });
            udpServer.OnServerStopped += new EventHandler<EventArgs>(delegate(object sender, EventArgs args) { WriteLogLine("[UDP Server] Server stopped"); });
            udpServer.OnClientConnected += new EventHandler<EventArgs>(delegate(object sender, EventArgs args) { WriteLogLine("[UDP Server] Client connected"); });
            udpServer.OnClientDisconnecting += new EventHandler<EventArgs>(delegate(object sender, EventArgs args) { WriteLogLine("[UDP Server] Client disconnecting"); });
            udpServer.OnClientDisconnected += new EventHandler<EventArgs>(delegate(object sender, EventArgs args) { WriteLogLine("[UDP Server] Client disconnected"); });
            udpServer.OnDataReceived += new EventHandler<EventArgs>(delegate(object sender, EventArgs args) { WriteLogLine("[UDP Server] Data received"); });

            tcpServer.Start();
            udpServer.Start();
        }

        private void StopNetworking()
        {
            tcpServer.Stop();

            udpServer.Stop();
        }


        private void MainLoop()
        {
            double t = 0.0;
            const double dt = 0.01;

            long currentTime = DateTime.Now.Ticks;
            double accumulator = 0.0;

            //State previous;
            //State current;

            while (state == (short)States.Running)
            {
                if (loopCallback != null)
                {
                    loopCallback.Invoke();
                }

                long newTime = DateTime.Now.Ticks;

                TimeSpan elapsedSpan = new TimeSpan(newTime - currentTime);
                
                double frameTime = elapsedSpan.TotalSeconds;

                if (frameTime > 0.25)
                    frameTime = 0.25;	  // max frame time to avoid spiral of death

                currentTime = newTime;

                accumulator += frameTime;

                while (accumulator >= dt)
                {
                    //previousState = currentState;
                    //integrate(currentState, t, dt);
                    t += dt;
                    accumulator -= dt;
                }

                double alpha = accumulator / dt;

                //State state = currentState * alpha + previousState * (1.0 - alpha);

                //render(state);
            }
        }

        private void WriteLog(String text)
        {
            if (logCallback != null)
            {
                logCallback.Invoke("[" + DateTime.Now.ToString() + "] " + text);
            }
        }

        private void WriteLogLine(String text)
        {
            WriteLog(text + "\r\n");
        }
    }
}
