﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MonoCraftServerLib;

namespace MonoCraftServerGui
{
    public partial class Main : Form
    {
        private MonoCraftServerLib.Server _server;

        public Main()
        {
            _server = new MonoCraftServerLib.Server();
            _server.loopCallback = new MonoCraftServerLib.Server.LoopCallback(this.ServerLoopCallback);
            _server.logCallback = new MonoCraftServerLib.Server.LogCallback(this.ServerLogCallback);

            InitializeComponent();
        }

        #region server callbacks

        private void ServerLogCallback(String text)
        {
            _WriteLog(text);
        }

        private void ServerLoopCallback()
        {
            Application.DoEvents();
        }

        #endregion

        private void _WriteLog(String text)
        {
            textBox1.AppendText(text);
        }

        #region events

        private void button2_Click(object sender, EventArgs e)
        {
            stopButton.Enabled = true;
            startButton.Enabled = false;

            _server.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            stopButton.Enabled = false;

            _server.Stop();

            startButton.Enabled = true;
        }

        #endregion
    }
}
