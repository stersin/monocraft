﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoCraftCommonLib
{
    abstract public class Entity
    {
        public int X { private set;get; }
        public int Y { private set; get; }
        public int Z { private set; get; }
    }
}
