﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace MonoCraftCommonLib.Net
{
    public class UdpNetworkServer : NetworkServer
    {
        private EndPoint _ipeSender;

        public UdpNetworkServer(String IpAddress, Int32 Port) : base(IpAddress, Port){}

        override protected Socket GetSocket()
        {
            return new Socket(this.Endpoint.AddressFamily, SocketType.Dgram, ProtocolType.Udp);
        }

        override protected void BeginReceiveBody(SocketConnectionInfo connection)
        {
            connection.Socket.BeginReceiveFrom(connection.Buffer, 0, connection.Buffer.Length, SocketFlags.None, ref _ipeSender, new AsyncCallback(DataReceived), connection);
        }

        override protected Int32 EndReceiveBody(SocketConnectionInfo connection, IAsyncResult asyncResult)
        {
            return connection.Socket.EndReceiveFrom(asyncResult, ref _ipeSender);
        }

        override protected void StartBody()
        {
            // So we can buffer and store information, create a new information class
            SocketConnectionInfo connection = new SocketConnectionInfo();
            connection.Buffer = new byte[SocketConnectionInfo.BufferSize];
            connection.Socket = _listener;
            // Setup the IPEndpoint
            _ipeSender = new IPEndPoint(IPAddress.Any, this.Port);
            // Start recieving from the client
            _listener.BeginReceiveFrom(connection.Buffer, 0, connection.Buffer.Length, SocketFlags.None, ref _ipeSender, new AsyncCallback(DataReceived), connection);
        }
    }
}
