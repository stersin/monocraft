﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace MonoCraftCommonLib.Net
{
    public class TcpNetworkServer : NetworkServer
    {
        public TcpNetworkServer(String IpAddress, Int32 Port) : base(IpAddress, Port) { }

        override protected Socket GetSocket()
        {
            return new Socket(this.Endpoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        }

        override protected void BeginReceiveBody(SocketConnectionInfo connection)
        {
            connection.Socket.BeginReceive(connection.Buffer, 0, connection.Buffer.Length, SocketFlags.None, new AsyncCallback(DataReceived), connection);
        }

        override protected Int32 EndReceiveBody(SocketConnectionInfo connection, IAsyncResult asyncResult)
        {
            return connection.Socket.EndReceive(asyncResult);
        }

        override protected void StartBody()
        {
            // Start listening to the socket, accepting any backlog
            _listener.Listen(this.MaxQueuedConnections);

            // Use the BeginAccept to accept new clients
            _listener.BeginAccept(new AsyncCallback(ClientConnected), _listener);
        }
    }
}
