﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace MonoCraftCommonLib.Net
{
    abstract public class NetworkServer
    {
        private Int32 _currentConnections = 0;
        protected Socket _listener;

        public Int32 Port { get; set; }
        public Int32 CurrentConnections { get { return _currentConnections; } }
        public Int32 MaxQueuedConnections { get; set; }
        public IPEndPoint Endpoint { get; set; }

        #region abstract methods

        abstract protected Socket GetSocket();
        abstract protected void BeginReceiveBody(SocketConnectionInfo connection);
        abstract protected Int32 EndReceiveBody(SocketConnectionInfo connection, IAsyncResult asyncResult);
        abstract protected void StartBody();

        #endregion

        #region constructors

        public NetworkServer() { }

        public NetworkServer(String IpAddress, Int32 Port)
        {
            IPAddress ip;

            // Check the IpAddress to make sure that it is valid
            if (IPAddress.TryParse(IpAddress, out ip))
            {
                this.Endpoint = new IPEndPoint(ip, Port);
              
                this.Port = Port;
            }
            else
            {
                throw new ArgumentException("The argument 'IpAddress' is not valid");
            }
        }

        #endregion

        #region Events

        public event EventHandler<EventArgs> OnServerStarting;
        public event EventHandler<EventArgs> OnServerStarted;
        public event EventHandler<EventArgs> OnServerStopping;
        public event EventHandler<EventArgs> OnServerStopped;
        public event EventHandler<EventArgs> OnClientConnected;
        public event EventHandler<EventArgs> OnClientDisconnecting;
        public event EventHandler<EventArgs> OnClientDisconnected;
        public event EventHandler<EventArgs> OnDataReceived;

        #endregion

        public void Start()
        {
            // Tell anything that is listening that we have starting to work
            if (OnServerStarting != null)
            {
                OnServerStarting(this, null);
            }

            // Get either a TCP or UDP socket depending on what we specified when we created the class
            _listener = GetSocket();

            // Bind the socket to the endpoint
            _listener.Bind(this.Endpoint);

            // TODO :: Add throttleling (using SEMAPHORE's)

            StartBody();

            // Tell anything that is listening that we have started to work
            if (OnServerStarted != null)
            {
                OnServerStarted(this, null);
            }
        }

        public void Stop()
        {
            if (OnServerStopping != null)
            {
                OnServerStopping(this, null);
            }

            if (OnServerStopped != null)
            {
                OnServerStopped(this, null);
            }
        }

        internal void ClientConnected(IAsyncResult asyncResult)
        {
            // Increment our ConcurrentConnections counter
            Interlocked.Increment(ref _currentConnections);

            // So we can buffer and store information, create a new information class
            SocketConnectionInfo connection = new SocketConnectionInfo();
            connection.Buffer = new byte[SocketConnectionInfo.BufferSize];

            // We want to end the async event as soon as possible
            Socket asyncListener = (Socket)asyncResult.AsyncState;
            Socket asyncClient = asyncListener.EndAccept(asyncResult);

            // Set the SocketConnectionInformations socket to the current client
            connection.Socket = asyncClient;

            // Tell anyone that's listening that we have a new client connected
            if (OnClientConnected != null)
            {
                OnClientConnected(this, null);
            }

            // TODO :: Add throttleling (using SEMAPHORE's)

            // Begin recieving the data from the client
            BeginReceiveBody(connection);

            // Now we have begun recieving data from this client,
            // we can now accept a new client
            _listener.BeginAccept(new AsyncCallback(ClientConnected), _listener);
        }

        internal void DataReceived(IAsyncResult asyncResult)
        {
            SocketConnectionInfo connection = (SocketConnectionInfo)asyncResult.AsyncState;
            Int32 bytesRead;
            // End the correct async process
            bytesRead = EndReceiveBody(connection, asyncResult);

            // Increment the counter of BytesRead
            connection.BytesRead += bytesRead;
            // Check to see whether the socket is connected or not...
            if (IsSocketConnected(connection.Socket))
            {
                // If we have read no more bytes, raise the data received event
                if (bytesRead < SocketConnectionInfo.BufferSize)
                {
                    byte[] buffer = connection.Buffer;
                    Int32 totalBytesRead = connection.BytesRead;
                    // Setup the connection info again ready for another packet
                    connection = new SocketConnectionInfo();
                    connection.Buffer = new byte[SocketConnectionInfo.BufferSize];
                    connection.Socket = ((SocketConnectionInfo)asyncResult.AsyncState).Socket;
                    // Fire off the receive event as quickly as possible, then we can process the data...
                    BeginReceiveBody(connection);

                    // Remove any extra data
                    if (totalBytesRead < buffer.Length)
                    {
                        Array.Resize<Byte>(ref buffer, totalBytesRead);
                    }
                    // Now raise the event, sender will contain the buffer for now
                    if (OnDataReceived != null)
                    {
                        OnDataReceived(buffer, null);
                    }
                    buffer = null;
                }
                else
                {
                    // Resize the array ready for the next chunk of data
                    Array.Resize<Byte>(ref connection.Buffer, connection.Buffer.Length + SocketConnectionInfo.BufferSize);
                    // Fire off the receive event again, with the bigger buffer
                    BeginReceiveBody(connection);
                }
            }
            else if (connection.BytesRead > 0)
            {
                // We still have data
                Array.Resize<Byte>(ref connection.Buffer, connection.BytesRead);
                // call the event
                if (OnDataReceived != null)
                {
                    OnDataReceived(connection.Buffer, null);
                }
            }
        }

        internal bool IsSocketConnected(Socket socket)
        {
            return !(socket.Poll(1, SelectMode.SelectRead) && socket.Available == 0);
        }

        internal void DisconnectClient(SocketConnectionInfo connection)
        {
            if (OnClientDisconnecting != null)
            {
                OnClientDisconnecting(this, null);
            }
            connection.Socket.BeginDisconnect(true, new AsyncCallback(ClientDisconnected), connection);
        }

        internal void ClientDisconnected(IAsyncResult asyncResult)
        {
            SocketConnectionInfo sci = (SocketConnectionInfo)asyncResult;
            sci.Socket.EndDisconnect(asyncResult);
            if (OnClientDisconnected != null)
            {
                OnClientDisconnected(this, null);
            }
        }
    }

    public class SocketConnectionInfo
    {
        public const Int32 BufferSize = 1048576;
        public Socket Socket;
        public byte[] Buffer;
        public Int32 BytesRead { get; set; }
    }
}
