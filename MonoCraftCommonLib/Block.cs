﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoCraftCommonLib
{
    #region BlockType

    public enum BlockType : byte
    {
        None,
        Air,
        Rock,
        Grass,
        Dirt,
        Water
    }

    #endregion

    #region Block

    public struct Block
    {
        public BlockType Type;

        public Block(BlockType blockType)
        {
            Type = blockType;
        }
    }

    #endregion
}
