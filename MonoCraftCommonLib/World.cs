﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoCraftCommonLib
{
    public class World
    {
        const short CHUNK_WIDTH = 16;
        const short CHUNK_HEIGHT = 16;
        const short CHUNK_DEPTH = 16;
        const short MAX_NUM_CHUNKS_WIDTH = 100;
        const short MAX_NUM_CHUNKS_HEIGHT = 10;
        const short MAX_NUM_CHUNKS_DEPTH = 100;

        const float WATER_HEIGHT = 0.5f;

        public String Name
        {
            private set;
            get;
        }

        public int Seed
        {
            private set;
            get;
        }

        public World(String name, int seed)
        {
            Name = name;
            Seed = seed;
        }
        
        public Block[] getChunkData(int chunkX, int chunkY, int chunkZ)
        {
            int noiseWidth = MAX_NUM_CHUNKS_WIDTH * CHUNK_WIDTH;
            int noiseDepth = MAX_NUM_CHUNKS_DEPTH * CHUNK_DEPTH;

            int mapHeight = MAX_NUM_CHUNKS_HEIGHT * CHUNK_HEIGHT;

            Block[] blocks = new Block[CHUNK_WIDTH * CHUNK_HEIGHT * CHUNK_DEPTH];


            return blocks;
        }
    }
}
